# Sensitive Variables
# source .env # Uncomment only if running something locally, but the pipeline needs it to be commented

# Google Credentials
# Only uncomment these variables if running locally, DON'T PUSH THESE VARIABLES to not break the CI
# export GOOGLE_CREDENTIALS_B64=$(cat infra/.keys/creds.json | base64 -w0)
export GOOGLE_APPLICATION_CREDENTIALS=".keys/creds.json" # path to creds.json from infra folder

{% if cookiecutter.allow_bitbucket_to_db == "False" %}
# Terraform Variables
export TF_VAR_authorized_networks=[{name="",value=""}]
{%- endif -%}
