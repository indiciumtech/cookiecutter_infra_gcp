# {{cookiecutter.project_name.replace('_', ' ').capitalize()}}

## General Informations

* Each module of the {{cookiecutter.project_name.replace('_', ' ').capitalize()}} project has its own repository and therefore its own CI/CD. Every CI must have at least two steps: Lint and Test. The CIs should use only one Makefile command whenever possible (sometimes it's not possible to build this way, but you should try to minimize the number of command lines of the CI);
* Repository's environment variables must have theis names exposed in the `.example.env` file. **DON'T EXHIBIT THE VALUE**, just the name. When any new variable is added, the `.example.env` file must be updated. The `.env` file containing the real values of the environment variables is present in `.gitignore` and is shared only through [Keeper Security](https://www.keepersecurity.com/). The `.env` should NOT be arbitrary modified;
* The Python Package Manager will always be updated to its latest version (`pip install -U pip`) before the installation of the requirements, and the requirements should try not to specify any library version. Much less errors occur when we let the package manager handle the libraries versions. In some cases, the version of a library may have to be defined because there is no backward compatibility of the requirements with old versions, but this is an isolated situation;
* All actions towards each module must have its respective command in the repository's Makefile and you should try to use Docker in all cases. The modules must follow the basic structure shown bellow:

```sh
    module_name/
    .env
    .example.env
    .gitignore
    bitbucket-pipelines.yml
    Dockerfile
    Makefile
    README.md
    requirements.txt
    setup.py
```

---------------------

## Repository Structure


------------

    ├── infra
    │   ├── infra.tf            <- Needed modules for the infrastructure.
    │   ├── provider.tf         <- Set AWS provider and tfstate to S3 backend.
    │   ├── firewall.tf         <- Security groups definitions.
    │   └── vars.tf             <- Module variables.
    │
    ├── README.md               <- The top-level README of the project.
    │
    ├── .example.env            <- Template for .env file, without variable declaration
    │
    ├── .gitignore              <- List of files to be ignored by git
    │
    ├── bitbucket-pipelines.yml <- Infra's deployment pipeline
    │
    └── project_conf.sh         <- Non-sensitive variables declaration

--------

## Setting Up the Infra

After you've created this repository both using just Cookiecutter or Cookiecutter with Ritchie, some things need to be tested or changed before merging these folders and files to master.

- The first step is to create the service account that will be deploying the resources from terraform and give this account the needed permissions. Place the generated `creds.json` at `infra/.keys/creds.json`

```bash
# Configure the project
$ gcloud config set project {{cookiecutter.project_id}}

# Create service account
$ gcloud iam service-accounts create --project {{cookiecutter.project_id}} indicium-terraform-{{cookiecutter.client_name}} --display-name "Terraform admin account for {{cookiecutter.client_name}} project"

# Create service account keys file
$ gcloud iam service-accounts keys create creds.json --iam-account indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com

# Give needed permissions to service account
$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/servicenetworking.serviceAgent

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/storage.admin

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/compute.securityAdmin

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/compute.networkAdmin

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/container.admin

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/compute.instanceAdmin.v1

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/iam.serviceAccountUser

$ gcloud projects add-iam-policy-binding {{cookiecutter.project_id}} --member serviceAccount:indicium-terraform-{{cookiecutter.client_name}}@{{cookiecutter.project_id}}.iam.gserviceaccount.com --role roles/cloudsql.admin

# Enable Google Apis 
gcloud services enable --project {{cookiecutter.project_id}} cloudresourcemanager.googleapis.com servicenetworking.googleapis.com compute.googleapis.com sqladmin.googleapis.com serviceusage.googleapis.com container.googleapis.com

```

- Secondly, create the bucket provided on the backend (see `infra/provider.tf`). Head to Google Cloud Storage (GCS) and create it with the same name listed on the file;

- Thirdly, get your **public** SSH key and copy it to `infra/.keys/ssh_pubkey.pub`;

- (Optional) If you've set `allow_bitbucket_to_db` to `false`, you will need to provide the allowed IP addresses at the variable `authorized_networks`, inside `infra/vars.tf`;

- (Optional) As default the **sql_database** resource `deletion_protection` is set as `true`. This don't allow the `terraform destroy` to remove this resourse. The reason for this is that you can't reuse the name of the deleted instance until one week from the deletion date.

- Also, you will need to allow an SSH Key from your repository to acccess `indicium_terraform`, so the pipeline could download its modules. To do so, just follow the folowing steps:

    1. Head to your online repository, go to **Repository Settings** (need repository's admin permission), **Pipeline Settings** and enable pipelines;

    2. After that, still in the **Repository Settings**, go to **SSH Keys**, create an SSH Key for this repository and copy it;

    3. Now go to `indicium_terraform` repository, **Repository Settings**, **Access Keys**, **Add Key** and paste the SSH Key you just copied with an intuitive label.

- Finally, head back to your repository settings and add the sensitive variables of the project to **Repository Variables**. The default ones are:
    - GOOGLE_CREDENTIALS_B64
    - SSH_PUBKEY_B64
    - TF_VAR_DB_USER
    - TF_VAR_DB_PASSWORD

\* To get GOOGLE_CREDENTIALS_B64 and SSH_PUBKEY_B64 values, just run and copy it to its respective variable

```sh
# GOOGLE_CREDENTIALS_B64
$ cat ./infra/.keys/creds.json | base64 -w0

# SSH_PUBKEY_B64
$ cat ./infra/.keys/ssh_pubkey.pub | base64 -w0
```

Now all you need to do is to commit this repository into your feature branch with the pipeline pointing to this exact branch, so you can test it. The pipeline will initialize Terraform, execute `terraform plan` and, with a manual trigger, execute the apply. Go to your repository and keep up with its progress. If the plan is what you are applying, simple trigger the run of the apply and check if it runs as expected.

If everything runs as expected, change the branch of the pipeline to `master` and create your PR.
