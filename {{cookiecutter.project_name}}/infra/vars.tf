variable "gcp_region" {
  type    = string
  default = "{{cookiecutter.gcp_region}}"
}

variable "project_id" {
  type    = string
  default = "{{cookiecutter.project_id}}"
}

variable "project_name" {
  type    = string
  default = "indicium"
}
{% if cookiecutter.allow_bitbucket_to_db == "False" %}
variable "authorized_networks" {
  type = list(object({
    name  = string
    value = string
  }))
}
{%- endif %}

variable "db_instance_name" {
  type    = string
  default = "postgres-indicium"
}

variable "DB_USER" {
  type = string
}

variable "DB_PASSWORD" {
  type = string
}

variable "gke_tags" {
  type    = list(string)
  default = ["gke-node"]
}

variable "ssh_keys" {
  type = list(object({
    user        = string
    pubkey_path = string
  }))
  default = [
    {
      user        = "indicium"
      pubkey_path = "./.keys/ssh_pubkey.pub"
    }
  ]
}
