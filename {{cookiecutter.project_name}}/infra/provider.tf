terraform {
  backend "gcs" {
    bucket = "indicium-{{cookiecutter.client_name}}-client"
    prefix = "terraform/infra/terraform.tfstate"
  }
}

provider "google" {
  region  = var.gcp_region
  project = var.project_id
}

resource "google_project_service" "service" {
  for_each = toset([
    "cloudresourcemanager.googleapis.com",
    "servicenetworking.googleapis.com",
    "compute.googleapis.com",
    "sqladmin.googleapis.com",
    "serviceusage.googleapis.com",
    "container.googleapis.com",
  ])

  project            = var.project_id
  service            = each.key
  disable_on_destroy = false
}
