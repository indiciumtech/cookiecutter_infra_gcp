#################################
# VPN Rules
#################################

resource "google_compute_firewall" "allow_access_to_vpn" {
  name    = "vpn-firewall"
  network = module.vpc.vpc_network

  allow {
    protocol = "udp"
    ports    = ["13315"]
  }

  target_tags   = ["vpn"]
}

resource "google_compute_firewall" "allow_default_https" {
  name    = "allow-https"
  network = module.vpc.vpc_network

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  target_tags   = ["https"]
}

resource "google_compute_firewall" "allow_default_http" {
  name = "allow-http"
  network = module.vpc.vpc_network

  allow {
    protocol = "tcp"
    ports = ["80"]
  }

  target_tags = ["http"]
}

resource "google_compute_firewall" "allow_ssh" {
  name    = "allow-ssh"
  network = module.vpc.vpc_network

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  target_tags   = ["ssh"]
}

#################################
# Kubernetes Rules
#################################
resource "google_compute_firewall" "kubernetes-master-https" {
  name    = "kubernetes-master-https"
  network = module.vpc.vpc_network

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
  
  target_tags = [ "gke-node" ]
  source_ranges = [ "191.191.96.44/32" ]  # Not sure this is needed, it looks we need https to the master api thus this rule
}

resource "google_compute_firewall" "kubernetes-ssh" {
  name    = "kubernetes-ssh"
  network = module.vpc.vpc_network

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  
  target_tags = [ "gke-node" ]
  source_ranges = [ "191.191.96.44/32" ]
}
