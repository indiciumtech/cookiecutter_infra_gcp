module "vpc" {
  source = "git::git@bitbucket.org:indiciumtech/indicium_terraform.git//modules/gcp/vpc?ref={{cookiecutter.module_version}}"

  project_name = var.project_name
  gcp_region   = var.gcp_region
}

module "sql_database" {
  source = "git::git@bitbucket.org:indiciumtech/indicium_terraform.git//modules/gcp/sql_database?ref={{cookiecutter.module_version}}"

  gcp_region          = var.gcp_region
  deletion_protection = true
  sql_instance        = "{{cookiecutter.sql_db_instance}}"
  vpc_network_id      = module.vpc.vpc_network_id
  db_instance_name    = var.db_instance_name
  db_user             = var.DB_USER
  db_password         = var.DB_PASSWORD
  vpn_ip_address      = module.vpn.vpn_ip_address
  authorized_networks = 
  {%- if cookiecutter.allow_bitbucket_to_db == "True" -%}
  [
    {
      name  = "BitBucket 1"
      value = "34.199.54.113/32"
    },
    {
      name  = "BitBucket 2"
      value = "34.232.25.90/32"
    },
    {
      name  = "BitBucket 3"
      value = "34.232.119.183/32"
    },
    {
      name  = "BitBucket 4"
      value = "34.236.25.177/32"
    },
    {
      name  = "BitBucket 5"
      value = "35.171.175.212/32"
    },
    {
      name  = "BitBucket 6"
      value = "52.54.90.98/32"
    },
    {
      name  = "BitBucket 7"
      value = "52.202.195.162/32"
    },
    {
      name  = "BitBucket 8"
      value = "52.203.14.55/32"
    },
    {
      name  = "BitBucket 9"
      value = "52.204.96.37/32"
    },
    {
      name  = "BitBucket 10"
      value = "34.218.156.209/32"
    },
    {
      name  = "BitBucket 11"
      value = "34.218.168.212/32"
    },
    {
      name  = "BitBucket 12"
      value = "52.41.219.63/32"
    },
    {
      name  = "BitBucket 13"
      value = "35.155.178.254/32"
    },
    {
      name  = "BitBucket 14"
      value = "35.160.177.10/32"
    },
    {
      name  = "BitBucket 15"
      value = "34.216.18.129/32"
    },
    {
      name  = "Pritunl"
      value = "${module.vpn.vpn_ip_address}/32"
    }
  ]
  {%- else -%}
  var.authorized_networks
  {%- endif %}
}

module "gke" {
  source = "git::git@bitbucket.org:indiciumtech/indicium_terraform.git//modules/gcp/gke?ref={{cookiecutter.module_version}}"

  project_name   = var.project_name
  gcp_region     = var.gcp_region
  vpc_network    = module.vpc.vpc_network
  vpc_subnetwork = module.vpc.vpc_subnetwork
  node_instance  = "{{cookiecutter.gke_node_instance}}"
  node_tags      = var.gke_tags
}

module "vpn" {
  source = "git::git@bitbucket.org:indiciumtech/indicium_terraform.git//modules/gcp/vpn?ref={{cookiecutter.module_version}}"

  gcp_region     = var.gcp_region
  vpn_instance   = "{{cookiecutter.vpn_instance}}"
  ssh_keys       = var.ssh_keys
  vpc_network    = module.vpc.vpc_network
  vpc_subnetwork = module.vpc.vpc_subnetwork
}
