# Cookiecutter Indicium GCP

Indicium's infra GCP project template

## Requirements

* Python 3.5+
* [Cookiecutter Package](cookiecutter.readthedocs.org/en/latest/installation.html), which can be installed with pip

```sh
$ pip install cookiecutter
```

## Starting a new project

To create a new templated infra project, just run:

```sh
$ cookiecutter git@bitbucket.org:indiciumtech/cookiecutter_infra_gcp.git
```

If you want to create using an alternative branch, just use the option -c followed by the branch name

```sh
$ cookiecutter -c branch_name git@bitbucket.org:indiciumtech/cookiecutter_infra_gcp.git
```

---------------------

## Resulting Directory

The resulting directory should look like this:


------------

    ├── infra
    │   ├── infra.tf            <- Needed modules for the infrastructure.
    │   ├── provider.tf         <- Set AWS provider and tfstate to S3 backend.
    │   ├── firewall.tf         <- Security groups definitions.
    │   └── vars.tf             <- Module variables.
    │
    ├── README.md               <- The top-level README of the project.
    │
    ├── .example.env            <- Template for .env file, without variable declaration
    │
    ├── .gitignore              <- List of files to be ignored by git
    │
    ├── bitbucket-pipelines.yml <- Infra's deployment pipeline
    │
    └── project_conf.sh         <- Non-sensitive variables declaration

--------
